# Lab 12: Web Requests, Caching, DataFrames and Scraping

In this lab, you'll get practice with downloading files from the web, analyzing data using `pandas`, and parsing data in HTML files.

-----------------------------
## Corrections/Clarifications

* 11/29/2022 2:30 PM - Typo in Task 3.1 fixed - redownload `practice.ipynb` for latest notebook.


**Find any issues?** Please report to us:

- Ashwin Maran <amaran@wisc.edu>
- Isha Padmanaban <ipadmanaban@wisc.edu>
- Brandon Tran <bqtran2@wisc.edu>

## Learning Objectives:

In this lab, you will practice how to:

* use HTTP requests to download content from the internet,
* cache data onto your computer,
* construct and modify DataFrames to analyze datasets,
* use `BeautifulSoup` to parse web pages and extract useful information.

------------------------------

## Note on Academic Misconduct

You may do these lab exercises only with your project partner; you are not allowed to start 
working on lab-p12 with one person, then do the project with a different partner. Now may be a 
good time to review [our course policies](https://cs220.cs.wisc.edu/f22/syllabus.html).

**Important:** p12 and p13 are two parts of the same data analysis.
You **cannot** switch project partners between these two projects.
If you partnered up with someone for p12, you have to sustain that partnership until the end of p13.
**You must acknowledge that you have read this to your lab TA**.

------------------------------

## Segment 0: Setup

Unlike previous labs, you will **not** be working on an Otter notebook in this lab. Most importantly, this means that `practice_test.py` will **not** be provided to you. There will be `assert` statements in your `practice.ipynb` notebook to guide you, but they will **not** be comprehensive. Instead, if you come across any syntactical or semantic errors, you will have to debug your code by yourself. Feel free to reach out to your TA or PM if you get stuck anywhere. and you will instead learn how to test your code by yourself.

You **will** however be provided with a `p12_test.py` for the project.

First, create a `lab-p12` directory and download the `practice.ipynb` file into the directory.

## Segments 1-3: Web Requests, Caching, DataFrames and Scraping

For the remaining segments, detailed instructions are provided in `practice.ipynb`. From the terminal, open a `jupyter notebook` session, open your `practice.ipynb` and follow the instructions in `practice.ipynb`.

## Project 12

You can now get started with [p12](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-f22-projects/-/tree/main/p12). **You may copy/paste any code created here in project p12**. Remember to only work on p12 with your partner from this point on. Have fun!
