# Project 11 (P11) grading rubric 

## Code reviews

- A TA / grader will be reviewing your code after the deadline.
- They will make deductions based on the Rubric provided below.
- To ensure that you don’t lose any points in code review, you must review the rubric and make sure that you have followed the instructions provided in the project correctly.

## Rubric

### General guidelines:

- Displaying excessive irrelevant information (-3)
- Required outputs not visible/did not save the notebook file prior to running the cell containing "export" (-3)
- Used concepts/modules (like csv.DictReader, pandas) not covered in class yet (built-in functions that you have been introduced to can be used) (-3)
- Defined required functions or data structures more than once (-3) 
- Looping through a dictionary to get the value of a key instead of just accessing that key's value (-3)
- Hardcoded indices (-3)
- `import` statements are not mentioned in the required cell at the top of the notebook or used additional import statements beyond those that are stated in the directions (-3)
- Used meaningless/built-in function names for variables (-3) 
- Repeating exact same code in multiple places instead of wrapping inside function (-3)

### Question specific guidelines:

- Q1 deductions 
	- Answer is hardcoded (-5)
	- Incorrect logic is used to answer (-2)

- Q2 deductions
	- Answer is hardcoded (-5)
	- Incorrect logic is used to answer (-2)

- Q3 deductions
	- Answer is hardcoded (-5)
	- Incorrect logic is used to answer (-2)

- Q4 deductions
	- Answer is hardcoded (-5)
	- Incorrect logic is used to answer (like missing `insolation_flux`, and `equilibrium_temperature` data are not ignored) (-2)
	- Plot is incorrect/not visible/not properly labeled (-2)

- Q5 deductions
	- Answer is hardcoded (-5)
	- Incorrect logic is used to answer (like missing `insolation_flux`, and `equilibrium_temperature` data are not ignored) (-2)
	- Plot is incorrect/not visible/not properly labeled (-2)

- Q6 deductions
	- Answer is hardcoded (-5)
	- `star_classes` data structure is not implemented/logic is incorrect (-3)
	- Incorrect logic is used to answer (-2)

- Q7 deductions
	- Answer is hardcoded (-5)
	- Incorrect logic is used to answer (-2)
	- Plot is incorrect/not visible/not properly labeled (-2)

- Q8 deductions
	- Answer is hardcoded (-5)
	- Incorrect logic is used to answer (-2)
	- Plot is incorrect/not visible/not properly labeled (-2)

- Q9 deductions
	- Answer is hardcoded (-5)
	- Incorrect logic is used to answer (like Star objects with any of the required four attributes missing are not ignored) (-2)
	- Plot is incorrect/not visible/not properly labeled (-2)

- Q10 deductions
	- Answer is hardcoded (-5)
	- `get_all_paths_in` function is not implemented/logic is incorrect (like files that start with "." are not ignored or output is not explicitly sorted in alphabetical order)/required functions (like `get_all_paths_in` is not used to answer) (-3)
	- Incorrect logic is used to answer (-2)
	- Incorrect input arguments are passed to the required functions (-1)

- Q11 deductions
	- Answer is hardcoded (-5)
	- Required functions (like `get_all_paths_in` is not used to answer) (-3)
	- Incorrect logic is used to answer (-2)
	- Incorrect input arguments are passed to the required functions (-1)

- Q12 deductions
	- Answer is hardcoded (-5)
	- Required functions (like `get_all_paths_in` is not used to answer) (-3)
	- Incorrect logic is used to answer (-2)
	- Incorrect input arguments are passed to the required functions (-1)

- Q13 deductions
	- Answer is hardcoded (-5)
	- Required functions (like `get_all_paths_in` is not used to answer) (-3)
	- Incorrect logic is used to answer (-2)
	- Incorrect input arguments are passed to the required functions (-1)

- Q14 deductions
	- Answer is hardcoded (-5)
	- `all_planets_list` data structure or or `get_surface_gravity` function is not implemented/logic is incorrect (like not returning None in `get_surface_gravity` function when either the `planet_mass` or `planet_radius` data is missing) (-3)
	- Incorrect logic is used to answer (-2)

- Q15 deductions
	- Answer is hardcoded (-5)
	- `get_distances_to_star` function is not implemented/logic is incorrect (like not returning None if eccentricity or `semi_major_radius` data is missing) (-3)
	- Incorrect logic is used to answer (-2)

- Q16 deductions
	- Answer is hardcoded (-5)
	- `get_liquid_water_distances` function is not implemented/logic is incorrect (like not returning None if `stellar_luminosity` data of the host Star object is missing) (-3)
	- Incorrect logic is used to answer (-2)

- Q17 deductions
	- Answer is hardcoded (-5)
	- Incorrect logic is used to answer (-2)

- Q18 deductions
	- Answer is hardcoded (-5)
	- `get_surface_temperatures` function is not implemented/logic is incorrect (like not returning None if `equilibrium_temperature` data of the Planet is missing) (-3)
	- Incorrect logic is used to answer (-2)

- Q19 deductions
	- Answer is hardcoded (-5)
	- Incorrect logic is used to answer (-2)

- Q20 deductions
	- Answer is hardcoded (-5)
	- Incorrect logic is used to answer (-2)

