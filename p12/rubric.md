# Project 12 (P12) grading rubric 

## Code reviews

- A TA / grader will be reviewing your code after the deadline.
- They will make deductions based on the Rubric provided below.
- To ensure that you don’t lose any points in code review, you must review the rubric and make sure that you have followed the instructions provided in the project correctly.

## Rubric

### General guidelines:

- Displaying excessive irrelevant information (-3)
- Required outputs not visible/did not save the notebook file prior to running the cell containing "export" (-3)
- Used concepts/modules (like csv.DictReader, numpy) not covered in class yet (built-in functions that you have been introduced to can be used) (-3)
- Defined required functions or data structures more than once (-3) 
- Looping through a dictionary to get the value of a key instead of just accessing that key's value (-3)
- Hardcoded indices (-3)
- `import` statements are not mentioned in the required cell at the top of the notebook or used additional import statements beyond those that are stated in the directions (-3)
- Used meaningless/built-in function names for variables (-3) 
- Repeating exact same code in multiple places instead of wrapping inside function (-3)

### Question specific guidelines:

- Q1 deductions 
	- Answer is hardcoded (-5)
	- `download` function is not implemented/logic is incorrect (like downloading the file even if it already exists)/required functions (like `download` function is not used for downloading `rankings.json` file) are not used/`rankings.json` is manually downloaded (-3)
	- Conditional statements or loops are used (-3)
	- Incorrect logic is used to answer (-2)
	- Incorrect input arguments are passed to the required functions (-1)

- Q2 deductions
	- Answer is hardcoded (-5)
	- Conditional statements or loops are used (-3)
	- Incorrect logic is used to answer (-2)

- Q3 deductions
	- Answer is hardcoded (-5)
	- Conditional statements or loops are used (-3)
	- Incorrect logic is used to answer (-2)

- Q4 deductions
	- Answer is hardcoded (-5)
	- `uw_madison` variable is not used to answer (-3)
	- Conditional statements or loops are used (like to go through a pandas DataFrame instead of boolean indexing) (-3)
	- `.loc` is used on a pandas DataFrame to hardcode an index instead of using `.iloc` (-2)
	- Incorrect logic is used to answer (-2)

- Q5 deductions
	- Answer is hardcoded (-5)
	- `uw_madison` variable is not used to answer (-3)
	- Conditional statements or loops are used (-3)
	- Incorrect logic is used to answer (-2)

- Q6 deductions
	- Answer is hardcoded (-5)
	- Conditional statements or loops are used (-3)
	- Incorrect logic is used to answer (-2)

- Q7 deductions
	- Answer is hardcoded (-5)
	- Conditional statements or loops are used (-3)
	- `.loc` is used on a pandas DataFrame to hardcode an index instead of using `.iloc` (-2)
	- Incorrect logic is used to answer (-2)

- Q8 deductions
	- Answer is hardcoded (-5)
	- Conditional statements or loops are used (-3)
	- `.loc` is used on a pandas DataFrame to hardcode an index instead of using `.iloc` (-2)
	- Incorrect logic is used to answer (-2)

- Q9 deductions
	- Answer is hardcoded (-5)
	- Conditional statements or loops are used (-3)
	- `.loc` is used on a pandas DataFrame to hardcode an index instead of using `.iloc` (-2)
	- Incorrect logic is used to answer (-2)

- Q10 deductions
	- Answer is hardcoded (-5)
	- Conditional statements or loops are used (-3)
	- `.loc` is used on a pandas DataFrame to hardcode an index instead of using `.iloc` (-2)
	- Incorrect logic is used to answer (-2)

- Q11 deductions
	- Answer is hardcoded (-5)
	- Conditional statements or loops are used (-3)
	- Incorrect logic is used to answer (-2)

- Q12 deductions
	- Answer is hardcoded (-5)
	- `institutions_df` data structure is not implemented/logic is incorrect/`institutions_df` is not used to answer (-3)
	- Conditional statements or loops are used (-3)
	- `.loc` is used on a pandas DataFrame to hardcode an index instead of using `.iloc` (-2)
	- Incorrect logic is used to answer (-2)

- Q13 deductions
	- Answer is hardcoded (-5)
	- `institutions_df` is not used to answer (-3)
	- Conditional statements or loops are used (-3)
	- `.loc` is used on a pandas DataFrame to hardcode an index instead of using `.iloc` (-2)
	- Incorrect logic is used to answer (-2)

- Q14 deductions
	- Answer is hardcoded (-5)
	- `institutions_df` is not used to answer (-3)
	- Conditional statements or loops are used (-3)
	- Incorrect logic is used to answer (-2)

- Q15 deductions
	- Answer is hardcoded (-5)
	- `year_2020_ranking_df` is not used to answer (-3)
	- Conditional statements or loops are used (-3)
	- `.loc` is used on a pandas DataFrame to hardcode an index instead of using `.iloc` (-2)
	- Incorrect logic is used to answer (-2)

- Q16 deductions
	- Answer is hardcoded (-5)
	- `year_2019_ranking_df` and `year_2021_ranking_df` are not used to answer (-3)
	- Conditional statements or loops are used (-3)
	- `.loc` is used on a pandas DataFrame to hardcode an index instead of using `.iloc` (-2)
	- Incorrect logic is used to answer (-2)

- Q17 deductions
	- Answer is hardcoded (-5)
	- Conditional statements or loops are used (-3)
	- Incorrect logic is used to answer (-2)

- Q18 deductions
	- Answer is hardcoded (-5)
	- `download` function is not used to download 2019-2020.html /2020-2021.html/ 2021-2022.html files (-3)
	- Incorrect logic is used to answer (-2)
	- Incorrect input arguments are passed to the required functions (-1)

- Q19 deductions
	- Answer is hardcoded (-5)
	- `parse_html` function is not implemented/logic is incorrect/`parse_html` function is not used to answer (-3)
	- Incorrect logic is used to answer (-2)
	- Incorrect input arguments are passed to the required functions (-1)

- Q20 deductions
	- Answer is hardcoded (-5)
	- Incorrect logic is used to answer (-2)

