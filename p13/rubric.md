# Project 13 (P13) grading rubric 

## Code reviews

- A TA / grader will be reviewing your code after the deadline.
- They will make deductions based on the Rubric provided below.
- To ensure that you don’t lose any points in code review, you must review the rubric and make sure that you have followed the instructions provided in the project correctly.

## Rubric

### General guidelines:

- Did not close the connection to `conn` at the end of notebook (-5)
- `import` statements are not mentioned in the required cell at the top of the notebook or used additional import statements beyond those that are stated in the directions (-3)
- Displaying excessive irrelevant information (-3)
- Required outputs not visible/did not save the notebook file prior to running the cell containing "export" (-3)
- Defined required functions/data structures more than once or repeating exact same code in multiple places instead of wrapping inside a function (-3) 
- Calling unnecessary functions/slow functions multiple times within a loop (-3)
- Hardcoded indices (-3)
- Used meaningless/built-in function names for variables (-3)
- Manually downloaded specific files (-3)
- Used absolute paths such as C://ms//cs220//p12 or hardcoded // or \\ in any of the paths (-3)

### Question specific guidelines:

- Q1 deductions 
	- Answer is hardcoded (-5)
	- `download` function is not used to download `QSranking.json` (-3)
	- `conn` data structure is not implemented/logic is incorrect (-3)
	- Used `pandas` or DataFrame slicing instead of using SQL query to answer (-3)
	- Incorrect logic is used to answer (-2)

- Q2 deductions 
	- Answer is hardcoded (-5)
	- Used `pandas` or DataFrame slicing instead of using SQL query to answer (-3)
	- Incorrect logic is used to answer (-2)

- Q3 deductions 
	- Answer is hardcoded (-5)
	- Used `pandas` or DataFrame slicing instead of using SQL query to answer (-3)
	- Incorrect logic is used to answer (-2)

- Q4 deductions 
	- Answer is hardcoded (-5)
	- Did not use SQL query to answer (-3)
	- `bar_plot` function is not used (-3)
	- Incorrect logic is used to answer (-2)
	- Plot is incorrect/not visible/not properly labeled (-2)
	- Incorrect input arguments are passed to the required functions (-1)

- Q5 deductions 
	- Answer is hardcoded (-5)
	- Used `pandas` or DataFrame slicing instead of using SQL query to answer (-3)
	- Incorrect logic is used to answer (-2)

- Q6 deductions 
	- Answer is hardcoded (-5)
	- `top_10_total_score` DataFrame from Q5 is not used/`bar_plot` function is not used (-3)
	- Incorrect logic is used to answer (-2)
	- Plot is incorrect/not visible/not properly labeled (-2)
	- Incorrect input arguments are passed to the required functions (-1)

- Q7 deductions 
	- Answer is hardcoded (-5)
	- Used `pandas` or DataFrame slicing instead of using SQL query to answer (-3)
	- Incorrect logic is used to answer (-2)

- Q8 deductions 
	- Answer is hardcoded (-5)
	- Used `pandas` or DataFrame slicing instead of using SQL query to answer (-3)
	- `scatter_plot` function is not used (-3)
	- Incorrect logic is used to answer (-2)
	- Plot is incorrect/not visible/not properly labeled (-2)
	- Incorrect input arguments are passed to the required functions (-1)

- Q9 deductions  
	- Answer is hardcoded (-5)
	- Used `pandas` or DataFrame slicing instead of using SQL query to answer (-3)
	- `scatter_plot` function is not used (-3)
	- Incorrect logic is used to answer (-2)
	- Plot is incorrect/not visible/not properly labeled (-2)
	- Incorrect input arguments are passed to the required functions (-1)

- Q10 deductions 
	- Answer is hardcoded (-5)
	- Used `pandas` or DataFrame slicing instead of using SQL query to answer (-3)
	- `scatter_plot` function is not used (-3)
	- Incorrect logic is used to answer (-2)
	- Plot is incorrect/not visible/not properly labeled (-2)
	- Incorrect input arguments are passed to the required functions (-1)

- Q11 deductions  
	- Answer is hardcoded (-5)
	- Did not use SQL query to answer (-3)
	- `pandas` operations other than `.corr`, `.loc` and `.iloc` are used (-3)
	- Incorrect logic is used to answer (-2)

- Q12 deductions 
	- Answer is hardcoded (-5)
	- Did not use SQL query to answer (-3)
	- `pandas` operations other than `.corr`, `.loc` and `.iloc` are used (-3)
	- Incorrect logic is used to answer (-2)

- Q13 deductions 
	- Answer is hardcoded (-5)
	- Used `pandas` or DataFrame slicing instead of using SQL query to answer (-3)
	- Incorrect logic is used to answer (-2)

- Q14 deductions 
	- Answer is hardcoded (-5)
	- Used `pandas` or DataFrame slicing instead of using SQL query to answer (-3)
	- Incorrect logic is used to answer (-2)

- Q15 deductions 
	- Answer is hardcoded (-5)
	- Used `pandas` or DataFrame slicing instead of using SQL query to answer (-3)
	- Incorrect logic is used to answer (-2)

- Q16 deductions 
	- Answer is hardcoded (-5)
	- Used `pandas` or DataFrame slicing instead of using SQL query to answer (-3)
	- `horizontal_bar_plot` function is not used (-3)
	- Incorrect logic is used to answer (-2)
	- Plot is incorrect/not visible/not properly labeled/countries in the horizontal bar plot are not ordered in increasing order of the difference between `avg_citations` and `avg_inter_faculty` (-2)
	- Incorrect input arguments are passed to the required functions (-1)

- Q17 deductions 
	- Answer is hardcoded (-5)
	- Used `pandas` or DataFrame slicing instead of using SQL query to answer (-3)
	- `regression_line_plot` function is not used (-3)
	- Incorrect logic is used to answer (-2)
	- Plot is incorrect/not visible/not properly labeled (-2)
	- Incorrect input arguments are passed to the required functions (-1)

- Q18 deductions 
	- Answer is hardcoded (-5)
	- `regression_line_plot` function is not used (-3)
	- Incorrect logic is used to answer (-2)
	- Plot is incorrect/not visible/not properly labeled (-2)
	- Incorrect input arguments are passed to the required functions (-1)

- Q19 deductions 
	- Answer is hardcoded (-5)
	- Incorrect logic is used to answer (-2)

- Q20 deductions 
	- Answer is hardcoded (-5)
	- `num_institutions` DataFrame from Q4 is not used/`pie_plot` function is not used (-3)
	- Incorrect logic is used to answer (-2)
	- Plot is incorrect/not visible/not properly labeled/not titled/legend is visible (-2)
	- Incorrect input arguments are passed to the required functions (-1)

